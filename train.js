const tf = require('@tensorflow/tfjs');
const fs = require('fs');
const ProgressBar = require('progress');



require('@tensorflow/tfjs-node-gpu');

//require('@tensorflow/tfjs-node');


const argparse = require('argparse');

const data = require('./data');



async function run(epochs, batchSize, modelSavePath) {
  await data.loadData();
  const model = await tf.loadModel("http://localhost:8080/model.json")


  const {images: trainImages, labels: trainLabels} = data.getTrainData();
  model.summary();

  const optimiser = 'adam';
  model.compile({
    optimizer: optimiser,
    loss: 'categoricalCrossentropy',
    metrics: ['accuracy'],
  });

  console.log("compiled")

  let epochBeginTime;
  let millisPerStep;
  const validationSplit = 0.15;
  const numTrainExamplesPerEpoch =
      trainImages.shape[0] * (1 - validationSplit);
  const numTrainBatchesPerEpoch =
      Math.ceil(numTrainExamplesPerEpoch / batchSize);


  console.log("train start")
  await model.fit(trainImages, trainLabels, {
    epochs,
    batchSize,
    validationSplit,
});

  const {images: testImages, labels: testLabels} = data.getTestData();
  const evalOutput = model.evaluate(testImages, testLabels);

 console.log(
     `\nEvaluation result:\n` +
     `  Loss = ${evalOutput[0].dataSync()[0].toFixed(3)}; `+
     `Accuracy = ${evalOutput[1].dataSync()[0].toFixed(3)}`);

  if (modelSavePath != null) {
    await model.save('file://cnnsave');
    console.log(`Saved model to path: ${modelSavePath}`);
  }

  fs.copyFile("~/Projects/trainer/cnnsave/model.json", "~/Projects/recogserver/model.json");
  fs.copyFile("~/Projects/trainer/cnnsave/weights.bin", "~/Projects/recogserver/weights.bin");
}

const parser = new argparse.ArgumentParser({
  description: 'EMNIST tensorflow model trainer',
  addHelp: true
});
parser.addArgument('--epochs', {
  type: 'int',
  defaultValue: 1,
  help: 'Number of epochs to train the model for.'
});
parser.addArgument('--batch_size', {
  type: 'int',
  defaultValue: 128,
  help: 'Batch size to be used during model training.'
})
parser.addArgument('--model_save_path', {
  type: 'string',
  defaultValue: './cnnsave',
  help: 'Path to which the model will be saved after training.'
});
const args = parser.parseArgs();



run(args.epochs, args.batch_size, args.model_save_path);
